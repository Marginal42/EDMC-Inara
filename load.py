# -*- coding: utf-8 -*-
import sys
import ttk
import Tkinter as tk
import requests

import myNotebook as nb
from config import config


this = sys.modules[__name__]
this.s = None
this.prep = {}


def plugin_start():
    this.email = tk.StringVar(value=config.get("InaraEmail"))
    this.password = tk.StringVar(value=config.get("InaraPassword"))
    this.online = False

    if login(this.email.get(), this.password.get()):
        this.online = True
    return 'Inara'

	
def plugin_app(parent):
    this.parent = parent
    label = tk.Label(parent, text="Inara:")
    this.status = tk.Label(parent, text="Disconnected", anchor=tk.W)
    if (this.online):
        this.status['text'] = "Logged into Inara"
    elif this.email.get():
        this.status['text'] = "Can't login"
    return (label, this.status)


def plugin_prefs(parent):
    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    email_label = nb.Label(frame, text="Inara account email")
    email_label.grid(padx=10, row=10, sticky=tk.W)

    email_entry = nb.Entry(frame, textvariable=this.email)
    email_entry.grid(padx=10, row=10, column=1, sticky=tk.EW)

    pass_label = nb.Label(frame, text="Password")
    pass_label.grid(padx=10, row=12, sticky=tk.W)

    pass_entry = nb.Entry(frame, textvariable=this.password, show=u'•')
    pass_entry.grid(padx=10, row=12, column=1, sticky=tk.EW)

    return frame

# Log in
def login(email, password):
    data = {
        "loginid": email,
        "loginpass": password,
        "loginremember": "1",
        "formact": "ENT_LOGIN",
        "location": "intro"
    }
    url = "https://inara.cz/login"
    this.s = requests.Session()
    r = this.s.post(url, data=data)
    return len(r.cookies) == 2

# Settings dialog dismissed
def prefs_changed():
    if this.email.get() != config.get("InaraEmail") or this.password.get() != config.get("InaraPassword") or not this.online:
        config.set("InaraEmail", this.email.get())
        config.set("InaraPassword", this.password.get())
        this.online = login(this.email.get(), this.password.get())
        this.status['text'] = this.online and "Logged into Inara" or "Can't login"

# Detect journal events
def journal_entry(cmdr, is_beta, system, station, entry, state):
    if is_beta:
        pass
    elif entry['event'] == 'Progress':
        setProgression(this.prep['Rank'], entry)
    elif entry['event'] == 'Rank':
        this.prep['Rank'] = entry
        print(this.prep)
    elif entry['event'] == 'CapiShipBond':
        addLog(2, "Received " + str(entry["Reward"]) + " from " + entry["AwardingFaction"] + " for kill a Capital Ship owned by " + entry["VictimFaction"])
    elif entry['event'] == 'Died':
        addLog(2, "Was killed by " + entry["KillerName"] + ", " + entry["KillerRank"] + " " + entry["KillerShip"])

# Update some data here too
def cmdr_data(data, is_beta):
    if not is_beta:
        cmdr_data.last = data
        setLocation(data["lastSystem"]["name"])
        setBalance(data["commander"]["credits"], 0)

# Defines location (system)
def setLocation(location):
    data = {
        "location": "cmdr",
        "formact": "USER_LOCATION_SET",
        "playercurloc": location
    }
    url = "https://inara.cz/cmdr"
    r = this.s.post(url, data=data)

# Defines balance & assets
def setBalance(credits, assets):
    data = {
        "location": "cmdr",
        "formact": "USER_CREDITS_SET",
        "playercredits": credits,
        "playercreditsassets": assets
    }
    url = "https://inara.cz/cmdr"
    r = this.s.post(url, data=data)

# Sets the current progression & ranks
def setProgression(r, p):
    data = {
        "location": "cmdr-ranks",
        "formact": "USER_USERRANKS_SET",
        "playerrep1[1]": r['Combat'],
        "playerrep1[2]": r['Trade'],
        "playerrep1[3]": r['Explore'],
        "playerrep1[4]": r['CQC'],
        "playerrep1[12]": r['Empire'],
        "playerrep1[13]": r['Federation'],
        "playerrep2[1]": p['Combat'],
        "playerrep2[2]": p['Trade'],
        "playerrep2[3]": p['Explore'],
        "playerrep2[4]": p['CQC'],
        "playerrep2[12]": p['Empire'],
        "playerrep2[13]": p['Federation'],
    }
    url = "https://inara.cz/cmdr-ranks"
    r = this.s.post(url, data=data)

# Adds an achievement log
# Types: 0 (Note), 1 (Exploration), 2 (Trading/Smuggling), 3 (Mission), 5 (Temporal), 6 (Financial)
def addLog(type, text):
    data = {
        "location": "cmdr-logs",
        "formact": "USER_ACHIEVEMENT_SET",
        "playerevent": "event5"+str(type),
        "playereventtext": text
    }
    url = "https://inara.cz/cmdr-logs"
    r = this.s.post(url, data=data)

cmdr_data.last = None
